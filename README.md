# cookiecutter-fastapi

Getting started with a new fast api project

## How to use it?

### Pre-requisites

### Create your new project from the template

If you do not have it yet, install `cookiecutter`:

```bash
pip install cookiecutter
```

Then, go to the directory where you want to have the project sources, and generate the
project from this template:

```bash
cookiecutter https://gitlab.com/toine-chevalier-cherel/cookiecutter-fastapi
```

You will be asked **how to name the project**.

You will also be asked about the **python minor version** that you will use in the
project (`3.6`, `3.7`, `3.8`, `3.9` ?). This will be used to configure the base Docker image
used in the gitlab CI jobs, as well as the pre-commit hooks (linter needs to know the
minor version in order to correctly detect invalid syntaxes).

### Setup your environment

First of all, create a new virtual environment (e.g. with `conda` or `pyenv`), and
activate it.

Then run the following commands, at the root of your project:

```bash
pip install -r requirements.txt -r requirements-dev.txt
git init
pre-commit install
```

Anytime you will want to run the pre-commits, run:

```bash
pre-commit run --all-files
```

Anytime you will want to run the tests, run:

```bash
pytest
```
