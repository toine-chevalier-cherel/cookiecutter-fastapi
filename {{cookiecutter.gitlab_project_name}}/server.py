from typing import Any, Dict

import uvicorn
import logging
from fastapi import APIRouter, FastAPI
from fastapi.responses import HTMLResponse
from fastapi.openapi.utils import get_openapi
from fastapi.staticfiles import StaticFiles

from src import constants as cst

logger = logging.getLogger()

# App declaration
STATIC_FOLDER = "static"
APP_TITLE = "{{cookiecutter.gitlab_project_name}}"
app = FastAPI(title=APP_TITLE, docs_url="/docs", redoc_url=None)

# Static volume
app.mount("/static", StaticFiles(directory=STATIC_FOLDER), name=STATIC_FOLDER)

# Customize app schema
def custom_shema() -> Dict[str, Any]:
    """Define FastApi app schema"""
    openapi_schema = get_openapi(
        title=APP_TITLE,
        version="1.0",
        routes=app.routes,
    )
    openapi_schema["info"] = {
        "title": APP_TITLE,
        "version": "1.0",
        "description": "",
        "contact": {
            "name": "Digital Lab",
            "email": "ts.digital-lab@totsa.com",
        },
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_shema


# On startup


@app.on_event("startup")
def on_startup() -> None:
    pass


# Frontend entrypoint

front_router = APIRouter()

@front_router.get("/{full_path:path}")
def load_app(full_path: str) -> HTMLResponse:
    with open("static/index.html") as f:
        content = f.read()
        f.close()
    return HTMLResponse(content)

app.include_router(
    front_router,
    prefix="/app",
    tags=["Frontend"]
)

# Backend entrypoints

back_router = APIRouter()


@back_router.get("/myendpoint", tags=["mytag"])
def myendpoint() -> None:
    pass

app.include_router(
    back_router,
    prefix="/api",
    tags=["Backeend"]
)

# Run

if __name__ == "__main__":
    uvicorn.run("server:app", port=cst.APP_PORT, reload=True)
